package rabbit

//Publisher subset of Client methods for publishing
type Publisher interface {
	PublishDataToQueue(queueName string, data interface{}) error
	Broadcast(exchangeName string, routingKey string, data interface{}) error
}

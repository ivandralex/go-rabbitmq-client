package rabbit

//Publisher subset of Client methods for publishing
type Publisher interface {
	PublishDataToQueue(queueName string, data interface{}) error
}

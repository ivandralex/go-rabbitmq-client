package rabbit

import (
	"encoding/json"

	"github.com/streadway/amqp"
	"golang.org/x/net/context"
)

//Session contains an amqp.Connection and an amqp.Channel
type Session struct {
	c       *amqp.Connection
	ch      *amqp.Channel
	Context context.Context
	done    context.CancelFunc
}

//DeclareExchange declares queue
func (s Session) DeclareExchange(name string, kind string) error {
	err := s.ch.ExchangeDeclare(
		name,  // name
		kind,  // kind
		true,  // durable
		false, // autoDelete
		false, // internal
		false, // noWait
		nil,   // args
	)

	return err
}

//DeclareQueue declares queue
func (s Session) DeclareQueue(queueName string, dlx string) error {
	var args amqp.Table

	if dlx != "" {
		args = make(amqp.Table)
		args["x-dead-letter-exchange"] = dlx
		args["x-dead-letter-routing-key"] = queueName
	}

	_, err := s.ch.QueueDeclare(
		queueName, // name
		true,      // durable
		false,     // delete when unused
		false,     // exclusive
		false,     // no-wait
		args,      // arguments
	)

	return err
}

//ConsumeQueue consumes queue
func (s Session) ConsumeQueue(queueName string) (<-chan amqp.Delivery, error) {
	return s.ch.Consume(
		queueName, // queue
		"",        // consumer
		false,     // auto-ack
		false,     // exclusive
		false,     // no-local
		false,     // no-wait
		nil,       // args
	)
}

//BindQueue binds queue to exchange
func (s Session) BindQueue(queueName string, routingKey string, exchange string) error {
	//log.Printf("bind queue %s to %s\n", queueName, exchange)
	return s.ch.QueueBind(queueName, routingKey, exchange, false, nil)
}

//PublishToQueue publishes []byte to queue
func (s Session) PublishToQueue(queueName string, body []byte) error {
	return s.ch.Publish("", queueName, false, false, amqp.Publishing{
		Body: body,
	})
}

//PublishDataToQueue publishes arbitrary struct to queue
func (s Session) PublishDataToQueue(queueName string, data interface{}) error {
	bytes, err := json.Marshal(data)

	if err != nil {
		return err
	}

	return s.PublishToQueue(queueName, bytes)
}

//Broadcast publishes arbitrary struct to exchange
func (s Session) Broadcast(exchangeName string, routingKey string, data interface{}) error {
	bytes, err := json.Marshal(data)

	if err != nil {
		return err
	}

	return s.ch.Publish(exchangeName, routingKey, false, false, amqp.Publishing{
		Body: bytes,
	})
}

//Close tears the connection down, taking the channel with it.
func (s Session) Close() error {
	if s.c == nil {
		return nil
	}

	return s.c.Close()
}

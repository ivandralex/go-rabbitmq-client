package rabbit

import (
	"fmt"
	"log"
	"time"

	"golang.org/x/net/context"

	"github.com/streadway/amqp"
)

//Config is just set of config parameters
type Config struct {
	Host                string
	Port                int
	User                string
	Password            string
	Vhost               string
	ReconnectionTimeout time.Duration
	PrefetchCount       int
}

//Client client to interact with RabbitMQ via amqp
type Client struct {
	context context.Context
	done    context.CancelFunc
}

//Connect connects to RabbitMQ
func (c *Client) Connect(ctx context.Context, config *Config) chan chan Session {
	c.context, c.done = context.WithCancel(ctx)
	sessions := make(chan chan Session)

	url := fmt.Sprintf("amqp://%s:%s@%s:%d%s", config.User, config.Password, config.Host, config.Port, config.Vhost)

	go func() {
		sess := make(chan Session)
		defer close(sessions)

		for {
			select {
			case sessions <- sess:
				//log.Println("put session channel to chan chan session channel")
			case <-c.context.Done():
				//log.Println("shutting down session factory")
				return
			}

			var conn *amqp.Connection
			var err error

			//Infinite reconnection loop
			for {
				conn, err = amqp.DialConfig(url, amqp.Config{
					Heartbeat: time.Second,
				})
				if err != nil {
					log.Printf("cannot (re)dial: %v: %q", err, url)
					time.Sleep(config.ReconnectionTimeout)
					continue
				}
				break
			}

			ch, err := conn.Channel()
			if err != nil {
				log.Fatalf("cannot create channel: %v", err)
			}

			if config.PrefetchCount != 0 {
				err = ch.Qos(1, 0, true)

				if err != nil {
					log.Fatalf("failed to set Qos: %v", err)
				}
			}

			//log.Println("AMQP Connected!")

			//Context for the session
			context, done := context.WithCancel(c.context)

			//Watch for connection closing event in a separate routine
			go func() {
				select {
				case <-context.Done():
					//log.Println("session context is done")
				case err := <-conn.NotifyClose(make(chan *amqp.Error)):
					log.Printf("closing AMQP connection: %s", err)
					done()
				}
			}()

			select {
			case sess <- Session{
				conn,
				ch,
				context,
				done,
			}:
				//log.Println("put new session in sessions chan")
			case <-context.Done():
				//log.Println("session context done")
			}
		}

	}()

	return sessions
}

//Disconnect from RabbitMQ
func (c *Client) Disconnect() {
	if c.done != nil {
		c.done()
	}
}

package rabbit

import (
	"encoding/json"
	"fmt"
	"log"

	"golang.org/x/net/context"

	"github.com/streadway/amqp"
)

// session composes an amqp.Connection with an amqp.Channel
type session struct {
	*amqp.Connection
	*amqp.Channel
}

//Client client to interact with RabbitMQ via amqp
type Client struct {
	context    context.Context
	done       context.CancelFunc
	sessionMap map[string]session
	sessions   chan chan session
}

func failOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
	}
}

// redial continually connects to the URL, exiting the program when no longer possible
func (c *Client) redial(url string) {
	c.sessions = make(chan chan session)
	go func() {
		sess := make(chan session)
		defer close(c.sessions)

		for {
			select {
			case c.sessions <- sess:
				//log.Println("put session to channel")
			case <-c.context.Done():
				log.Println("shutting down session factory")
				return
			}

			//log.Println("redial")

			conn, err := amqp.DialConfig(url, amqp.Config{
				Heartbeat: 0,
			})
			if err != nil {
				log.Fatalf("cannot (re)dial: %v: %q", err, url)
			}

			ch, err := conn.Channel()
			if err != nil {
				log.Fatalf("cannot create channel: %v", err)
			}

			//log.Println("Channel created")

			sess <- session{conn, ch}
		}
	}()
}

func (c *Client) getSession(queueName string) session {
	if _, ok := c.sessionMap[queueName]; !ok {
		//log.Println("Create new session")
		sess := <-c.sessions
		c.sessionMap[queueName] = <-sess
	}

	return c.sessionMap[queueName]
}

//Connect connect to rabbit mq
func (c *Client) Connect(host string, port int, user string, password string, vhost string) {
	c.sessionMap = make(map[string]session)
	c.context, c.done = context.WithCancel(context.Background())

	url := fmt.Sprintf("amqp://%s:%s@%s:%d%s", user, password, host, port, vhost)
	c.redial(url)
}

//Disconnect from rabbit mq
func (c *Client) Disconnect() {
	// c.session.Connection.Close()
	// c.session.Channel.Close()
	c.done()
}

//DeclareExchange declares queue
func (c *Client) DeclareExchange(name string, kind string) error {
	s := c.getSession(name)

	err := s.Channel.ExchangeDeclare(
		name,  // name
		kind,  // kind
		true,  // durable
		false, // autoDelete
		false, // internal
		false, // noWait
		nil,   // args
	)

	return err
}

//DeclareQueue declares queue
func (c *Client) DeclareQueue(queueName string, dlx string) error {
	s := c.getSession(queueName)

	var args amqp.Table

	if dlx != "" {
		args = make(amqp.Table)
		args["x-dead-letter-exchange"] = dlx
		args["x-dead-letter-routing-key"] = queueName
	}

	_, err := s.Channel.QueueDeclare(
		queueName, // name
		true,      // durable
		false,     // delete when unused
		false,     // exclusive
		false,     // no-wait
		args,      // arguments
	)

	return err
}

//ConsumeQueue consumes queue
func (c *Client) ConsumeQueue(queueName string) (<-chan amqp.Delivery, error) {
	s := c.getSession(queueName)

	return s.Channel.Consume(
		queueName, // queue
		"",        // consumer
		false,     // auto-ack
		false,     // exclusive
		false,     // no-local
		false,     // no-wait
		nil,       // args
	)
}

//BindQueue binds queue to exchange
func (c *Client) BindQueue(queueName string, exchange string) error {
	//log.Printf("bind queue %s to %s\n", queueName, exchange)
	s := c.getSession(queueName)
	return s.Channel.QueueBind(queueName, "", exchange, false, nil)
}

//PublishToQueue publishes []byte to queue
func (c *Client) PublishToQueue(queueName string, body []byte) error {
	s := c.getSession(queueName)

	return s.Channel.Publish("", queueName, false, false, amqp.Publishing{
		Body: body,
	})
}

//PublishDataToQueue publishes arbitrary struct to queue
func (c *Client) PublishDataToQueue(queueName string, data interface{}) error {
	bytes, err := json.Marshal(data)

	if err != nil {
		return err
	}

	return c.PublishToQueue(queueName, bytes)
}
